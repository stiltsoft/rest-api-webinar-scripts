import requests
import csv
import sys

bitbucket_url = sys.argv[1]
login = sys.argv[2]
password = sys.argv[3]
since = sys.argv[4]
until = sys.argv[5]

bb_api_url = bitbucket_url + '/rest/api/latest'
ag_api_url = bitbucket_url + '/rest/awesome-graphs-api/latest'

s = requests.Session()
s.auth = (login, password)


class UserWithStatistics:

    def __init__(self, user_slug, commits, loc_added, loc_deleted):
        self.user_slug = user_slug
        self.commits = commits
        self.loc_added = loc_added
        self.loc_deleted = loc_deleted


def get_user_list():

    user_list = []

    is_last_page = False

    while not is_last_page:

        url = bb_api_url + '/users'
        response = s.get(url, params={'start': len(user_list), 'limit': 25}).json()

        for user in response['values']:
            user_list.append(user['slug'])

        is_last_page = response['isLastPage']

    return user_list


def get_user_with_statistics(user_slug):

    url = ag_api_url + '/users/' + user_slug + '/commits/statistics'
    response = s.get(url, params={'sinceDate': since, 'untilDate': until}).json()

    commits = response['commits']
    loc_added = response['linesOfCode']['added']
    loc_deleted = response['linesOfCode']['deleted']

    return UserWithStatistics(user_slug, commits, loc_added, loc_deleted)


with open('user_commits_statistics_{}_{}.csv'.format(since, until), mode='a', newline='') as report_file:
    
    report_writer = csv.writer(report_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    report_writer.writerow(['user_slug', 'commits', 'loc_added', 'loc_deleted'])

    users_with_stats = map(get_user_with_statistics, get_user_list())
    sorted_users = sorted(users_with_stats, key=lambda user: user.commits, reverse=True)

    for user in sorted_users:
        report_writer.writerow([user.user_slug, user.commits, user.loc_added, user.loc_deleted])

print('The resulting CSV file is saved to the current folder.')
