import sys
import requests
import csv

bitbucket_url = sys.argv[1]
login = sys.argv[2]
password = sys.argv[3]
since = sys.argv[4]
until = sys.argv[5]

s = requests.Session()
s.auth = (login, password)


class Commit:

    def __init__(self, commit_id, created, author_email, author_name, repository_name, repository_slug, project_name,
                 project_key, loc_added, loc_deleted):

        self.commit_id = commit_id
        self.created = created
        self.author_email = author_email
        self.author_name = author_name
        self.repository_name = repository_name
        self.repository_slug = repository_slug
        self.project_name = project_name
        self.project_key = project_key
        self.loc_added = loc_added
        self.loc_deleted = loc_deleted


def get_commits():

    commits_list = []

    is_last_page = False

    url = f'{bitbucket_url}/rest/awesome-graphs-api/latest/commits'

    while not is_last_page:

        response = s.get(url, params={'sinceDate': since,
                                      'untilDate': until,
                                      'start': len(commits_list),
                                      'limit': 1000}).json()

        for commit in response['values']:

            commit_id = commit['id']
            created = commit['authorTimestamp']
            author_email = commit['author']['emailAddress']
            author_name = commit['author']['displayName']
            repository_name = commit['repository']['name']
            repository_slug = commit['repository']['slug']
            project_name = commit['repository']['project']['name']
            project_key = commit['repository']['project']['key']
            loc_added = commit['linesOfCode']['added']
            loc_deleted = commit['linesOfCode']['deleted']

            commits_list.append(Commit(commit_id, created, author_email, author_name, repository_name, repository_slug,
                                       project_name, project_key, loc_added, loc_deleted))

        is_last_page = response['isLastPage']

    return commits_list


def get_commit_message(project_key, repository_slug, commit_id):

    url = f'{bitbucket_url}/rest/api/latest/projects/{project_key}/repos/{repository_slug}/commits/{commit_id}'

    response = s.get(url).json()

    message = response['message']
    one_line_message = message.replace('\n', ' | ')

    return one_line_message


list_of_commits = get_commits()


with open(f'commits_{since}_{until}.csv', mode='a', newline='', encoding='utf16') as report_file:

    report_writer = csv.writer(report_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    report_writer.writerow(['Created Date', 'Author Email', 'Author Name', 'Repository Name', 'Project Name',
                            'Commit ID', 'Added Lines', 'Deleted Lines', 'Commit Message'])

    for commit in list_of_commits:

        print('Processing commit', commit.commit_id)

        created = commit.created
        author_email = commit.author_email
        author_name = commit.author_name
        repository_name = commit.repository_name
        project_name = commit.project_name
        commit_id = commit.commit_id
        loc_added = commit.loc_added
        loc_deleted = commit.loc_deleted
        commit_message = get_commit_message(commit.project_key, commit.repository_slug, commit.commit_id)

        report_writer.writerow([created, author_email, author_name, repository_name, project_name, commit_id, loc_added,
                                loc_deleted, commit_message])

print('The resulting CSV file is saved to the current folder.')
