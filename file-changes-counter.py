import sys
import requests
import csv
from collections import Counter

bitbucket_url = sys.argv[1]
login = sys.argv[2]
password = sys.argv[3]
project = sys.argv[4]
repository = sys.argv[5]
since = sys.argv[6]
until = sys.argv[7]

s = requests.Session()
s.auth = (login, password)


def get_commit_ids():

    commit_ids = []

    is_last_page = False

    url = f'{bitbucket_url}/rest/awesome-graphs-api/latest/projects/{project}/repos/{repository}/commits'

    while not is_last_page:

        response = s.get(url, params={'sinceDate': since, 'untilDate': until, 'start': len(commit_ids), 'limit': 1000}).json()

        for commit_data in response['values']:

            commit_id = commit_data['id']

            commit_ids.append(commit_id)

        is_last_page = response['isLastPage']

    return commit_ids


def get_files(commit_id):

    print('Processing commit', commit_id)

    files_in_commit = []

    url = f'{bitbucket_url}/rest/awesome-graphs-api/latest/projects/{project}/repos/{repository}/commits/{commit_id}'

    response = s.get(url, params={'withFiles': 'true'}).json()

    for changed_file in response['changedFiles']:

        changed_file_path = changed_file['path']

        files_in_commit.append(changed_file_path)

    return files_in_commit


commit_ids_list = get_commit_ids()

commit_files_list = []

for commit in commit_ids_list:

    commit_files_list.extend(get_files(commit))

changes_counter = Counter(commit_files_list)


with open(f'{project}_{repository}_changes_{since}_{until}.csv', mode='a', newline='') as report_file:

    report_writer = csv.writer(report_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    report_writer.writerow(['file_path', 'changes_number'])

    for change in changes_counter.most_common():

        file_path = change[0]
        changes = change[1]

        report_writer.writerow([file_path, changes])

print('The resulting CSV file is saved to the current folder.')
